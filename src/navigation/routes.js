import { Navigation } from "react-native-navigation";
import LoginScreen from "../screens/auth/LoginScreen";
import RegisterScreen from "../screens/auth/RegisterScreen";

export function routes(){
    Navigation.registerComponent('LoginScreen', ()=>LoginScreen)
    Navigation.registerComponent('RegisterScreen', ()=>RegisterScreen)
}