import React, { Component } from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  useColorScheme,
  View,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import colors from '../../utilities/colors';

class LoginScreen extends Component {

  static options() {
    return {
      topBar: {
        visible:false,
      },
    };
  }

  constructor(props){
    super(props);
    this.state = {
      phoneNumber: '',
      password:''
    };
  }

  _onLogin() {
    console.log('masuk');
  }
  _onRegister() {
    //console.log('daftar');
    Navigation.push(this.props.componentId, {
      component: {
        name: 'RegisterScreen',
      }
    })
  }

  render() {
    return (
      <ScrollView keyboardShouldPersistTaps={'handled'}>
        <Image
          source={require('../../assets/asetLogin.png')}
          style={styles.logo}
        />
        <TextInput
          style={styles.input}
          value={this.state.phoneNumber}
          onChangeText={(phoneNumber) => this.setState({ phoneNumber })}
          placeholder="Masukan Nomor HP"
          keyboardType="numeric"
        />
        <TextInput
          style={styles.input}
          value={this.state.password}
          onChangeText={(password) => this.setState({ password })}
          placeholder="Masukan Password"
          keyboardType="default"
          autoCapitalize='none'
          secureTextEntry 
        />
        
        <TouchableOpacity
         style={styles.button}
         onPress={() => this._onLogin()}>
           <Text style={styles.textButton}>MASUK</Text>
        </TouchableOpacity>

        <Text style={styles.dontHaveAcount}>
          Tidak Punya Akun?
        </Text>

        <TouchableOpacity 
          onPress={() => this._onRegister()}>
          <Text style={styles.registerButton}>DAFTAR</Text>
        </TouchableOpacity>
      </ScrollView>

    );
  };
}


const styles = StyleSheet.create({
  logo: {
    width: 160,
    height: 160,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginVertical: 32
  },
  input: {
    height: 40,
    marginHorizontal: 16,
    marginVertical:8,
    borderBottomWidth: 1,
    padding: 10,
  },
  button: {
    alignItems:"center",
    backgroundColor: colors.primaryColor,
    padding: 12,
    margin: 16,
    marginVertical: 20,
    borderRadius: 8
  },
  textButton: {
    color: colors.WhiteColor,
    fontWeight:'bold'
  },
  dontHaveAcount: {
    textAlign: 'center'
  },
  registerButton: {
    color:Colors.primaryColor,
    textAlign:'center',
    marginVertical: 16
  }
});

export default LoginScreen;
